
function validateId(data) {
  var re = /^[0-9]{1,15}$/;
  return re.test(data);
}

function validateName(data) {
  var re = /^[a-zA-ZА-Яа-я ]{1,30}$/;
  return re.test(data);
}

function validateArticle(data) {
  var re = /^[a-zA-ZА-Яа-я0-9()\-_.,:;#$%!^*+=?<>'"\\/@ ]{1,30}$/;
  return re.test(data);
}

function validateFloat(data) {
  var re = /^[0-9.]{1,15}$/;
  return re.test(data);
}

function validateInt(data) {
  var re = /^[0-9]{1,15}$/;
  return re.test(data);
}

function validateData(data, jsonName) {
  switch (jsonName) {
    case 'id':
      return validateId(data);

    case 'name':
      return validateName(data);

    case 'article':
      return validateArticle(data);

    case 'time':
      return validateFloat(data);

    case 'amount':
    case 'boxes':
    case 'inBox':
      return validateInt(data);

    default:
      console.log("chn-validation / switch:default = Error Parameter");
      return false
  }
}

export function validate(newData, jsonName, errorText, callback) {
  let data = newData.trim();

  let val = validateData(data, jsonName);

  console.log(`chn-validation / input ${jsonName} changed and validation: ${val}"`);

  let result = {[`${jsonName}ErrorText`]: ''};

  if (val == true) {
    result = {...result, [jsonName]: data}
    callback(result); // callback action- inputChanged
    return result;
  } else {
    result = {...result, [`${jsonName}ErrorText`]: `Validation Error ${jsonName}`}
  }

  // call action?setState(by return 1) only once, when error changed
  if (errorText != result[`${jsonName}ErrorText`]) {
    callback(result); // callback action- inputChanged
    return result;
  }

  return 'DenySetState'; // errorText again
}
