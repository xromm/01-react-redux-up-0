import React, {Component} from 'react'

import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';

import style from './answer.css'

export default class Answer extends Component {
	constructor(props) {
		super(props);
		console.log(' ========= RENDER APP/ANSWER: constructor ', this.props);

		this.state = {
      showWarning: false,
			warningMessage: "Error"
    };
	}
	render() {
		console.log(" ========= RENDER APP/ANSWER!");

		const {answer} = this.props;
		const {actions} = this.props;
		console.log(this.props);

		let resetAnswerParamerter;

		let btnText = "Написать сообщение";
		if (answer.answer != -1) {
			btnText = `Насписать ответ к ${answer.answer}`
			resetAnswerParamerter = <RaisedButton label={'Сброс ответа'} primary={true} onTouchTap={:: this.handleTouchTapReset} style={{marginTop: '1em'}}/>
		}

		return <div>
			<Paper className={style.Paper} zDepth={2}>

				<TextField defaultValue={answer.text} floatingLabelText="Новое сообщение" multiLine={true} rows={2} fullWidth={true} onChange={::this.handleTextChange}/>
				<RaisedButton label={btnText} primary={true} onTouchTap={:: this.handleTouchTap}/>
				{resetAnswerParamerter}

				<Snackbar
          open={this.state.showWarning}
          message={this.state.warningMessage}
          autoHideDuration={4000}
          onRequestClose={::this.handleHideWarning}
        />
			</Paper>
		</div>
	}

	handleTextChange(e) {
		console.log(e.target.value);
		this.setState({newText: e.target.value});
	}

	handleTouchTapReset() {
		this.props.actions.actionResetNewMessageData();
	}

	handleHideWarning = () => {
    this.setState({
      showWarning: false,
    });
  };

	handleTouchTap(e) {
		console.log(' addNewMessage = onTouchTap');

		try {

			let number = this.props.lastNumber;
			let answer = (this.props.answer.answer == undefined)? -1: this.props.answer.answer;
			let text = this.state.newText;
			let margin = (this.props.answer.margin == undefined)? 0: this.props.answer.margin;

			if ((text === undefined) || (text.length < 1))
				throw new Error('Пожалуйста введите сообщение');

			let data = {
				number: number,
				answer: answer,
				text: text,
				margin: margin
			}

			console.log(data);
			this.props.actions.actionAddNewMessage(data);

		} catch (e) {
			console.log(' addNewMessage = onTouchTap: ERROR', e.message);
			this.setState({
				showWarning: true,
				warningMessage: e.message
			});
		}
	}
}

Answer.PropTypes = {
	highlighted: React.PropTypes.bool
	//number: PropTypes.number.isRequired
}
