import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin();

import Forum from '../../components/Forum/Forum'
import * as AppActions from '../../actions/AppActions'

class App extends Component {
	render() {
		console.log(" ========= RENDER APP!");
		const {app} = this.props;
		const {appActions} = this.props;

		console.log(this.props);
		return <div>
			<Forum app={app} appActions={appActions} />
		</div>
	}
}

function mapStateToProps(state) {
	return {
    app: state.app
  }
}

function mapDispatchToProps(dispatch) {
	let appActions = bindActionCreators(AppActions, dispatch);
	console.log(" ========= APP mapDispatchToProps ");
	console.log(appActions);
	return {
    appActions: appActions
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

// Page.propTypes = {
// 	year: PropTypes.number.isRequired,
//
// 	photos: PropTypes.array.isRequired,
// 	fetching: PropTypes.bool.isRequired,
//   newIncYear: PropTypes.number.isRequired,
//
// 	pageActions: PropTypes.arrayOf(PropTypes.func)
// }
