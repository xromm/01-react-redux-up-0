import React, {Component} from 'react'

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import {validate} from '../../libs/nich-validation'

import style from './messages.css'
import styleAll from '../styleAll.css'

export default class Messages extends Component {
	constructor(props) {
		super(props);

		console.log(' ======= APP/FORUM/MESSAGES: constructor ', this.props);

		//this.props.headerActions.actionGetNewHeaderData();
	}

	// Prevents location change
  preventLocationChange (index, margin) {
		console.log("click", index, margin);
		let data = {
			answer: index,
			margin: margin+1
		}
		console.log(data);
		this.props.actions.actionSendNewAnswerData(data);
  }

	render() {
		console.log(" ========= RENDER APP/FORUM/MESSAGES");
		const messages = this.props.messages;
		let preventLocationChange = ::this.preventLocationChange;
		let massagesForm;

    if (messages.length > 0) {
      massagesForm = messages.map(function(item, index) {
				console.log('Render Messages ', item.text);
				let number = item.number;
				let margin = (item.margin < 4)? item.margin: 4;

				let paperStyle = {
					display: 'flex',
					flexFlow: 'column',
					marginLeft: `${margin * 1.5}em`,
					width: 'auto',
					marginTop: '0.5em',
					padding: '0.5em'
				}
				let textStyle = {
					flex: 1,
					width: 'auto'
				}
				let btnStyle = {
					marginLeft: `1em`,
					flex: 1,
					width: '150px'
				}

				let answerTo = (item.answer != -1)? `, ответ сообщению ${item.answer}`:'';

        return (
					<Paper zDepth={1} key={index} style={paperStyle}>
						<TextField hintText="Message Field" floatingLabelText={`Сообщение ${item.number}${answerTo}`} multiLine={true} rows={1} fullWidth={true} defaultValue={item.text} style={textStyle}/>
						<RaisedButton label="Ответить" primary={true} onClick={() => preventLocationChange(item.number, item.margin)} style={btnStyle}/>
          </Paper>
        )
      })
    } else {
      massagesForm = <p>К сожалению сообщений нет.</p>
    }

		return <div>
			<Paper className={styleAll.Paper,
			style.Paper} zDepth={2}>

				{massagesForm}

			</Paper>
		</div>
	}
}

Messages.PropTypes = {
	highlighted: React.PropTypes.bool
	//number: PropTypes.number.isRequired
}
