var webpack = require('webpack')
var webpackDevMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
var webpackConfig = require('./webpack.config')

var app = new (require('express'))()

// webpack hmre
var webpackCompiler = webpack(webpackConfig)
app.use(webpackDevMiddleware(webpackCompiler, {
	noInfo: true,
	publicPath: webpackConfig.output.publicPath,
	stats: {
		colors: true,
		hash: false,
		version: false,
		timings: false,
		assets: false,
		chunks: false,
		modules: false,
		reasons: false,
		children: false,
		source: false,
		errors: true,
		errorDetails: true,
		warnings: false,
		publicPath: false
	}
	//'errors-only'
}))
app.use(webpackHotMiddleware(webpackCompiler))

// body-pareser
var bodyParser = require('body-parser');
// body-parser
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded

var PORT = 3000;
app.listen(PORT, function() {
	console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", PORT);
});

// Middleware
app.use(function mainPage(req, res, next) {
	if (req.url == "/") {
		console.log(" SERVER = request: " + req.url);
		res.sendFile(__dirname + '/index.html');
	} else {
		next();
	}
});

// Otherwise
app.use(function notFoundPage(req, res) {
	res.status(404).send("Извините, страница не найдена.");
})

process.once('SIGUSR2', function() {
	gracefulShutdown(function() {
		process.kill(process.pid, 'SIGUSR2');
	});
});
