import React, {Component} from 'react'

import Answer from '../../components/Answer/Answer'
import Messages from '../../components/Messages/Messages'

export default class Forum extends Component {
  constructor(props) {
    super(props);
    console.log(' ========= RENDER APP/FORUM: constructor ', this.props);
    //this.props.submitArticleActions.actionGetSubmitBtnState();
  }

  render() {
		console.log(" ========= RENDER APP/FORUM!");
		const {answer, messages} = this.props.app;
		const {appActions} = this.props;

		let lastNumber = messages.length;
		let { margin, answerNumber } = messages;
		console.log(` lastNumber ${lastNumber}, margin ${margin}, answer ${answer}`);

		console.log(this.props);
		return <div>
			<Messages messages={messages} actions={appActions} />
			<Answer answer={answer} actions={appActions} lastNumber={lastNumber} margin={margin} answerNumber={answerNumber} />
		</div>
	}
}
