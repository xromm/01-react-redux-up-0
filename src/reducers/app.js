import {
	SEND_NEW_ANSWER_DATA,
	ADD_NEW_MESSAGE,
	RESET_NEW_MESSAGE_DATA
} from '../constants/App'

const initState = {
	answer: {
		answer: -1,
		text: ''
	},
	messages: [
		{
			number: 0,
			answer: -1,
			margin: 0,
			text: `Сообщение 0 текст текст текст ...
новая строка 1`
		}, {
			number: 1,
			answer: 0,
			margin: 1,
			text: `Сообщение 0 текст текст текст ...
новая строка 1
новая строка 2`
		}, {
			number: 2,
			answer: 1,
			margin: 2,
			text: 'Сообщение 2 текст текст текст ...'
		}, {
			number: 3,
			answer: 0,
			margin: 1,
			text: 'Сообщение 3 текст текст текст ...'
		}
	]
}

export default function app(state = initState, action) {

	switch (action.type) {
		case SEND_NEW_ANSWER_DATA:
			return {
				...state,
				answer: {
					answer: action.payload.answer,
					margin: action.payload.margin
				}
			}

		case RESET_NEW_MESSAGE_DATA:
			return {
				...state,
				answer: initState.answer
			}

		case ADD_NEW_MESSAGE: {
			let messages = state.messages;
			if (action.payload.answer == -1) {
				return {
					...state,
					messages: [...state.messages, action.payload]
				}
			}

			console.log(messages);

			for (var key in messages){
				if (messages[key].number == action.payload.answer) {
					let newKey = Number(key) + 1;
					console.log("splice ", Number(key)+1, messages[key].number);

					messages.splice(newKey, 0, action.payload);
					break
				}
			}
			console.log(messages);

			console.log("app.js ", messages);
			return {
				...state,
				messages: messages
			}
		}

		default: return state;
	}
}
