import {
	ADD_NEW_MESSAGE,
	SEND_NEW_ANSWER_DATA,
	RESET_NEW_MESSAGE_DATA
} from '../constants/App'

export function actionAddNewMessage(data) {
	return {
		type: ADD_NEW_MESSAGE,
		payload: data
	}
}

export function actionSendNewAnswerData(data) {
	return {
		type: SEND_NEW_ANSWER_DATA,
		payload: data
	}
}

export function actionResetNewMessageData(data) {
	return {
		type: RESET_NEW_MESSAGE_DATA,
		payload: data
	}
}
