var path = require('path')
var webpack = require('webpack')
var NpmInstallPlugin = require('npm-install-webpack-plugin')
var autoprefixer = require('autoprefixer')
var precss = require('precss')

module.exports = {
	devtool: 'cheap-module-eval-source-map',
	entry: [
		'webpack-hot-middleware/client',
		'babel-polyfill',
		'./src/index'
	],
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/static/'
	},
	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new NpmInstallPlugin()
	],
	resolve: {
    extensions: ['', '.js', '.jsx']
  },
	module: { //Обновлено
		preLoaders: [ //добавили ESlint в preloaders
			{
				exclude: [
					path.resolve(__dirname, "node_modules"),
					path.resolve(__dirname, "server"),
				],
				test: /\.jsx?$/,
				loaders: ['eslint'],
				include: [
					path.resolve(__dirname, "src"),
				],
			}
		],
		loaders: [ //добавили babel-loader
			{
				exclude: [
					path.resolve(__dirname, "node_modules"),
				],
				loaders: ['react-hot', 'babel-loader'],
				include: [
					path.resolve(__dirname, "src"),
				],
				test: /\.jsx?$/,
				plugins: ['transform-runtime'],
			},
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader?modules=true&localIdentName=[name]__[local]___[hash:base64:5]'
			}
			// {
			//   test:   /\.(scss|css)$/,
			//   loader: "style-loader!css-loader!postcss-loader"
			// }
		]
	},
	postcss: function() {
		return [autoprefixer, precss];
	}
}
